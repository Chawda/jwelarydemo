package demo.vishal.com.jwelarydemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

public class JwelaryActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jwelary);
    }

    @Override
    protected void onStart() {
        super.onStart();
        recyclerView = findViewById(R.id.recycler_jwel);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
        JwelaryAdapter adapter = new JwelaryAdapter(getApplicationContext());
        recyclerView.setAdapter(adapter);
    }
}
